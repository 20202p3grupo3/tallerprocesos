#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>


int main(int argc, char **argv){

	for(int i=0;i<argc;i++){
		pid_t id = fork();
		if(id == 0){
			char buf[100];
			memset(buf,0,100);
			snprintf(buf,100,"%d.png",i);
			execl("procesador_png",argv[i],buf,NULL);
			printf("La ejecución falló\n");		
		}
	}

	for(int j=0;j<argc;j++){
		int status;
		pid_t finalizado = wait(&status);
		printf("%s  (archivo BW %d.png) : status %d \n",argv[j],j,WEXITSTATUS(status));
	}
}
