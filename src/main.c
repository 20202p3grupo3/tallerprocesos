#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>


int main(int argc, char **argv){
	char buf[100];
	for(int i=1;i<argc;i++){
		memset(buf,0,100);
		pid_t id = fork();
		snprintf(buf,100,"%d.png",i);
		if(id == 0){			
			execl("procesador_png","procesador_png",argv[i],buf,NULL);
			printf("La ejecución falló\n");		
		}
	}

	for(int i=1;i<argc;i++){
		int status;
		memset(buf,0,100);
		snprintf(buf,100,"%d.png",i);
		pid_t finalizado = wait(&status);
		printf("%s  (archivo BW %s.png) : status %d \n",argv[i],buf,WEXITSTATUS(status));
	}
}
